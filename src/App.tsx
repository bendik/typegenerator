import React, {useState, useReducer} from 'react';

import './App.css';

interface AppProps {}

// naming a little silly and maybe more advanced uniontypes 
type RypeKeyPairs = {
  name: string,
  value: Rype
}

type RypeObj = {
  name: string,
  value: 'object',
  child: RypeKeyPairs[]
}

type RypeArray = {
  name: string,
  value: 'array',
  child: Rype
}

type RypeString = {
  name: string,
  value: 'string'
  child: never
}

type RypeNumber = {
  name: string,
  value: 'number',
  child: never
}

type Rype = RypeString | RypeNumber | RypeArray | RypeObj
type ReducerData = {k: string, v?: any}

function TypeSelect(t: Rype): React.ReactElement {
  const [type, setType] = useState<Rype | {name: '', value: 'string'}>(t)
  // need to pass some 
  return (
    <div style={{paddingLeft: '1em'}}>
      < label htmlFor="name" > Name</label >
      <input name="name" value={type.name} onInput={(event: React.ChangeEvent<HTMLInputElement>) => setType({...type, name: event.target.value})} />

      <select name="type" defaultValue={type.value} onChange={eventSetType}>
        <option value="string">String</option>
        <option value="number">Number</option>
        <option value="array">Array</option>
        <option value="object">Object</option>
      </select>
      {type.value === 'array'
        ?
        <>
          <TypeSelect type={type.child} />
          <input type="hidden" name="array-end" />
        </>
        : null}

      {
        type.value === 'object'
          ?
          <>
            {type.child.map((s, i) => ((<TypeSelect key={i} type={s} parent={type} />)))}
            <input type="hidden" name="object-end" />
            <button onClick={(e) => {
              e.preventDefault()
              setType({...type, child: [...type.child, {name: '', value: 'string'}]})
            }}>+</button>
          </>
          : null
      }
    </div >
  )

  function eventSetType(event: React.ChangeEvent<HTMLSelectElement>) {
    switch (event?.target?.value) {
      case 'object': setType({...type, value: event.target.value, child: []}); break
      case 'array': setType({...type, value: event.target.value, child: {name: '', value: 'string'}}); break
      default: setType({...type, value: event.target.value || 'string'})
    }
  }
}

function App(): React.ReactNode {
  const [type, dispatch] = useReducer(formDataReducer, '')
  return (
    <div className="App">
      <header className="App-header">
        <form onInput={formDataDispatcher} action="/typelexicon">
          <TypeSelect />
        </form>
      </header>
      <code>
        {type}
      </code>
    </div>
  )

  function formDataDispatcher(event: React.ChangeEvent<HTMLFormElement>) {
    const data: FormData = new FormData(event.currentTarget)
    dispatch({k: 'reset'})
    for (const [k, v] of data.entries()) {
      dispatch({k, v})
    }
  }
}

function formDataReducer(type: string, data: ReducerData): string {
  // TADA, the amounts of 'if's here just spells that i'm doing this wrong
  if (data.k === 'reset') return ''
  if (data.k === 'object-end') type += '}'
  if (data.k === 'array-end') type += '>'
  if (data.k === 'type') {
    if (data.v === 'string') type += data.v
    if (data.v === 'number') type += data.v
    if (data.v === 'array') type += '<'
    if (data.v === 'object') type += '{'
  } else if (data.k === 'name') {
    if (type[type.length - 1] === '{') type += data.v

  }

  return type
}

export default App;
